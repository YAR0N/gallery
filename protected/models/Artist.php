<?php

/**
 *   Artist - model to store artist`s data in mongodb.
 */

class Artist extends EMongoDocument {

    public $surName;
    public $firstName;
    public $middleName;

    public $description;

    public $phone;
    public $skype;
    public $email;

    /** @virtual */
    public $img;

    public function collectionName()
    {
        return 'artists';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function rules()
    {
        return array(
            array('surName,firstName,middleName,description,email', 'required'),
            array('surName,firstName,middleName,description,phone,skype,email', 'type', 'type' => 'string'),
            array('surName,firstName,middleName', 'length', 'max' => 20),
            array('description', 'length', 'max' => 500),
            array('skype', 'match', 'pattern' => '/^[a-zA-Z][a-zA-Z0-9\.,\-_]{5,31}$/'),
            array('phone', 'match', 'pattern' => '/^0?(39|50|63|66|67|68|91|92|93|94|95|96|97|98|99)+?\d{7}$/'),
            array('email', 'email', 'checkMX' => TRUE),

            array('img', 'file', 'mimeTypes' => 'image/jpeg', 'allowEmpty' => true, 'safe' => true, 'on' => 'update'),
            array('img', 'file', 'mimeTypes' => 'image/jpeg', 'allowEmpty' => false, 'safe' => true, 'on' => 'insert'),
        );
    }

    public function attributeNames()
    {
        return array('surName', 'firstName', 'middleName', 'description', 'skype', 'phone', 'email');
    }

    public function attributeLabels()
    {
        return array(
            'firstName'     => 'First Name',
            'surName'       => 'Surname',
            'middleName'    => 'Middle name',
            'description'   => 'Description',
            'skype'         => 'Skype',
            'phone'         => 'Phone Number',
            'email'         => 'E-mail',
            'img'           => 'Image'
        );
    }

    public function getFullName()
    {
        return $this->surName . " " . $this->firstName . " " . $this->middleName;
    }

    public function getPictures()
    {
        $models = Picture::model()->findAll(array('artistId' => (string) $this->_id));
        $categories = array();
        foreach($models as $model){
            $category = $model->category->name;
            if(array_key_exists($category, $categories)) {
                array_push($categories[$category], $model);
            } else {
                $categories[$category] = array($model);
            }
        }
        return $categories;
    }

    protected function afterSave()
    {
        $image = EMongoFile::populate($this,'img');
        if(!is_null($image)) {
            $oldImage = EMongoFile::model()->findOne(array('entityId' => $this->_id));
            if(!is_null($oldImage)) $oldImage->delete();
            $image->entityId = $this->_id;
            return $image->save();
        }
    }

    protected function beforeDelete()
    {
        if(Picture::model()->exists(array('artistId' => (string) $this->_id))){
            return false;
        }
        $image = EMongoFile::model()->findOne(array('entityId' => $this->_id));
        if(is_null($image) || $image->delete()) {
            return parent::beforeDelete();
        } else {
            return false;
        }
    }

}