<?php
/**
 *  Category - model to store categories in mongodb.
 */

class Category extends EMongoDocument {

    public $name;

    public function collectionName()
    {
        return 'categories';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function rules()
    {
        return array(
            array('name', 'required'),
            array('name', 'type', 'type' => 'string'),
            array('name', 'length', 'max' => 20)
        );
    }

    public function attributeNames()
    {
        return array('name');
    }

    public function attributeLabels()
    {
        return array(
            'name'     => 'Name',
        );
    }

    public function getPictures()
    {
       return Picture::model()->findAll(array('categoryId' => (string) $this->_id));
    }

    protected function beforeDelete()
    {
        if(Picture::model()->deleteAll(array('categoryId' => (string) $this->_id))){
            return parent::beforeDelete();
        } else {
            return false;
        }
    }
} 