<?php
/**
 * Picture - model to store pictures data in mongodb
 */

class Picture extends EMongoDocument {

    public $name;
    public $description;
    public $price;

    public $categoryId;
    public $artistId;

    /** @virtual */
    public $img;

    public function collectionName()
    {
        return 'pictures';
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function rules()
    {
        return array(
            array('name,description,price,categoryId,artistId', 'required'),
            array('name,description', 'type', 'type' => 'string'),
            array('name', 'length', 'max' => 20),
            array('description', 'length', 'max' => 500),
            array('price', 'match', 'pattern' => '/^\d+[\.,]?\d{0,2}$/'),

            array('artistId', 'EMongoExistValidator', 'className' => 'Artist', 'mongoId' => true, 'attributeName' => '_id'),
            array('categoryId', 'EMongoExistValidator', 'className' => 'Category', 'mongoId' => true, 'attributeName' => '_id'),

            array('img', 'file', 'mimeTypes' => 'image/jpeg', 'allowEmpty' => true, 'safe' => true, 'on' => 'update'),
            array('img', 'file', 'mimeTypes' => 'image/jpeg', 'allowEmpty' => false, 'safe' => true, 'on' => 'insert'),
        );
    }

    public function attributeNames()
    {
        return array('name', 'description', 'price', 'category', 'artist');
    }

    public function attributeLabels()
    {
        return array(
            'name'        => 'Name',
            'description' => 'Description',
            'price'       => 'Price',
            'category'    => 'Category',
            'artist'      => 'Artist',
            'img'         => 'Image'
        );
    }

    public function getArtist()
    {
        return Artist::model()->findOne(array('_id' => new MongoId($this->artistId)));
    }

    public function getCategory()
    {
        return Category::model()->findOne(array('_id' => new MongoId($this->categoryId)));
    }

    protected function afterSave()
    {
        $image = EMongoFile::populate($this,'img');
        if(!is_null($image)) {
            $oldImage = EMongoFile::model()->findOne(array('entityId' => $this->_id));
            if(!is_null($oldImage)) $oldImage->delete();
            $image->entityId = $this->_id;
            return $image->save();
        }
    }

    protected function beforeDelete()
    {
        $image = EMongoFile::model()->findOne(array('entityId' => $this->_id));
        if(is_null($image) || $image->delete()) {
            return parent::beforeDelete();
        } else {
            return false;
        }
    }

} 