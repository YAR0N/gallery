<?php $this->pageTitle = Yii::app ()->name . ' - Picture'; ?>

<div>
<?php
if(Yii::app()->user->hasFlash('message')) {
   echo CHtml::encode(Yii::app()->user->getFlash('message'));
}
?>
</div>

<?php

echo CHtml::link('Add picture', $this->createUrl('picture/create'));

$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $dataProvider,
    'columns' => array(
        'name:html',
        array(
            'name' => 'category',
            'value' => '$data->category->name'
        ),
        array(
            'name' => 'artist',
            'value' => '$data->artist->fullName'
        ),
        'price:html',
        array(
            'class'=>'CButtonColumn'
        )
    )
));
?>