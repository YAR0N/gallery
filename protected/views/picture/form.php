<?php
$this->pageTitle = Yii::app ()->name . ' - Picture';

$form = $this->beginWidget ('CActiveForm', array(
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array(
        'name' => 'artist',
        'enctype' => 'multipart/form-data'
    )
)); ?>

<div class="row">
    <?php echo $form->textField($model, 'name', array(
        'placeholder' => $model->getAttributeLabel('name'),
    )); ?>
    <?php echo $form->error($model,'name'); ?>
</div>

<div class="row">
    <?php echo $form->textArea($model, 'description', array(
        'placeholder' => $model->getAttributeLabel('description'),
    )); ?>
    <?php echo $form->error($model,'description'); ?>
</div>

<div class="row">
    <?php echo $form->textField($model, 'price', array(
        'placeholder' => $model->getAttributeLabel('price'),
    )); ?>
    <?php echo $form->error($model,'price'); ?>
</div>

<div class="row">
    <?php echo $form->dropDownList($model, 'artistId',
        CHtml::listData($artists,
        function($artist){
            return (string) $artist->_id;
        },
        function($artist){
            return CHtml::encode($artist->fullName);
        }),
        array(
            'prompt' => $model->getAttributeLabel('artist'),
        )
    ); ?>
    <?php echo $form->error($model,'artistId'); ?>
</div>

<div class="row">
    <?php echo $form->dropDownList($model, 'categoryId',
        CHtml::listData($categories,
            function($category){
                return (string) $category->_id;
            },
            function($category){
                return CHtml::encode($category->name);
            }),
        array(
            'prompt' => $model->getAttributeLabel('category')
        )
    ); ?>
    <?php echo $form->error($model,'categoryId'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'img'); ?>
    <?php echo $form->fileField($model, 'img'); ?>
    <?php echo $form->error($model,'img'); ?>
</div>

<?php echo CHtml::submitButton ('Save'); ?>

<?php $this->endWidget (); ?>