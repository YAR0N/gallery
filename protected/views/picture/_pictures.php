<div class="row-fluid">

    <ul class="portfolio thumbnails ">

        <?php foreach ($pictures as $picture): ?>
        <li class="span6">
            <a href="<?php echo $this->createUrl('picture/view', array('id' => $picture->_id)) ?>" rel="lightbox"  class="thumbnail"><?php echo CHtml::image($this->createUrl('site/image', array('id' => $picture->_id)), "No image for this picture", array('class' => 'img-polaroid')); ?></a>
        </li>
        <?php endforeach; ?>
    </ul>


</div><!-- /row-fluid -->
