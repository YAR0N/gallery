<?php $this->pageTitle = Yii::app ()->name . ' - ' . $model->name; ?>

<div class="page-header">
    <h1><?php echo CHtml::encode($model->name) ?></h1>
</div>


<div class="blog-image"><?php echo CHtml::image($this->createUrl('site/image', array('id' => $model->_id)), "No image for this picture", array('class' => 'img-polaroid')); ?></div>

<div class="row-fluid">
    <div class="span12">
        <h3 class="header"><?php echo $model->getAttributeLabel('description') ?>
            <span class="header-line"></span>
        </h3>
        <?php echo CHtml::encode($model->description) ?>
    </div>

</div><!--/row-fluid-->

<div class="row-fluid">
    <div class="span12" id="contactInfo">
        <h3 class="header">Price - <?php echo CHtml::encode($model->price) ?>



<?php echo CHtml::ajaxButton('Buy', $this->createUrl('artist/jsonData', array('id' => $model->artistId)),
    array(
        'dataType' => 'json',
        'error' => 'js:function(obj, err) {
            $("#contactInfo").append("Artist contacts can`t be reached");
        }',
        'success' => 'js:function(data) {
            $("#contactInfo").append("<div>Contacts:</div><div>E-mail: "+data.email+"</div><div>Phone: "+data.phone+"</div><div>Skype: "+data.skype+"</div>");
            $("#contact").hide();
        }'
    ),
    array(
        'id' => 'contact',
        'class' => 'btn btn-primary'
    )
    );
?>
            <span class="header-line"></span>
        </h3>
    </div>

</div><!--/row-fluid-->

<div class="row-fluid">
    <div class="span12">
        <h3 class="header">Artist - <?php echo CHtml::link(CHtml::encode($model->artist->fullName), $this->createUrl('artist/view', array('id' => $model->artistId))) ?>
    <span class="header-line"></span>
    </h3>
</div>

</div><!--/row-fluid-->

<div class="row-fluid">
    <div class="span12">
        <h3 class="header">Category - <?php echo CHtml::link(CHtml::encode($model->category->name), $this->createUrl('category/view', array('id' => $model->categoryId))) ?>
    <span class="header-line"></span>
    </h3>
</div>