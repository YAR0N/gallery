<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<div class="row-fluid">
    <div class="span12">
        <h3 class="header">Some pictures
            <span class="header-line"></span>
        </h3>
        <?php $this->renderPartial('//picture/_pictures', array('pictures' => $pictures)) ?>
    </div>

</div><!--/row-fluid-->

<div class="row-fluid">
    <h3 class="header">Categories
        <span class="header-line"></span>
    </h3>
    <ul class="thumbnails center">
        <?php foreach($categories as $category): ?>
            <li class="span3">
                <div class="thumbnail">
                    <?php echo CHtml::link(CHtml::encode($category->name), $this->createUrl('category/view', array('id' => $category->_id))); ?>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
</div>

<div class="row-fluid">
    <h3 class="header">Artists
        <span class="header-line"></span>
    </h3>
    <ul class="thumbnails center">
        <?php foreach($artists as $artist): ?>
            <li class="span3">
                <div class="thumbnail">
                    <?php echo CHtml::link(CHtml::encode($artist->fullName), $this->createUrl('artist/view', array('id' => $artist->_id))); ?>
                </div>
            </li>
        <?php endforeach; ?>
    </ul>
</div>