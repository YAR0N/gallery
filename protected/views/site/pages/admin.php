<?php


$this->pageTitle=Yii::app()->name . ' - Admin';

?>

<div class="page-header">
    <h1>Admin</h1>
</div>

<div class="row-fluid">
    <ul class="thumbnails center">
        <li class="span3">
            <div class="thumbnail">
<?php echo CHtml::link('Pictures', '/picture/admin'); ?>
            </div>
        </li>
        <li class="span3">
            <div class="thumbnail">
<?php echo CHtml::link('Categories', '/category/admin'); ?>
            </div>
        </li>
        <li class="span3">
            <div class="thumbnail">
<?php echo CHtml::link('Artists', '/artist/admin'); ?>
            </div>
        </li>
</ul>
</div>
