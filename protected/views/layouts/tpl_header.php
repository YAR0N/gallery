<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <?php $this->pageTitle = Yii::app ()->name; ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
	
    <!-- the styles -->
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/bootstrap-responsive.min.css">
    <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Pontano+Sans'>
    <link rel="stylesheet" type="text/css" href="/js/nivo-slider/themes/default/default.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/js/nivo-slider/nivo-slider.css" >
    <link rel="stylesheet" type="text/css" href="/js/lightbox/css/lightbox.css" />
    <link rel="stylesheet" type="text/css" href="/css/template.css">
    <link rel="stylesheet" type="text/css" href="/css/style1.css" />
    <link rel="alternate stylesheet" type="text/css" media="screen" title="style2" href="/css/style2.css" />
    <link rel="alternate stylesheet" type="text/css" media="screen" title="style3" href="/css/style3.css" />
    <link rel="alternate stylesheet" type="text/css" media="screen" title="style4" href="/css/style4.css" />
    <link rel="alternate stylesheet" type="text/css" media="screen" title="style5" href="/css/style5.css" />
    <link rel="alternate stylesheet" type="text/css" media="screen" title="style6" href="/css/style6.css" />
    
    <script type="text/javascript" src="/js/swfobject/swfobject.js"></script>
	<script type="text/javascript" src="/js/lightbox/js/lightbox.js"></script>
    <!-- style switcher -->
    <script type="text/javascript" src="/js/styleswitcher.js"></script>
    

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    


  </head>

<body>
<section id="header">
<!-- Include the header bar -->
    <?php include_once('header.php');?>
<!-- /.container -->  
</section><!-- /#header -->