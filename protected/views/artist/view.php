<?php $this->pageTitle = Yii::app ()->name . ' - ' . $model->fullName; ?>

<div class="page-header">
    <h1><?php echo CHtml::encode($model->fullName) ?></h1>
</div>


<div class="blog-image"><?php echo CHtml::image($this->createUrl('site/image', array('id' => $model->_id)), "No image for this picture", array('class' => 'img-polaroid')); ?></div>

<div class="row-fluid">
    <div class="span12">
        <h3 class="header"><?php echo $model->getAttributeLabel('description') ?>
            <span class="header-line"></span>
        </h3>
        <?php echo CHtml::encode($model->description) ?>
    </div>

</div><!--/row-fluid-->

<?php foreach($model->pictures as $category=>$pictures ): ?>

    <div class="row-fluid">
        <div class="span12">
            <h3 class="header"><?php echo CHtml::encode($category) ?>
                <span class="header-line"></span>
            </h3>
            <?php $this->renderPartial('//picture/_pictures', array('pictures' => $pictures)); ?>
        </div>

    </div><!--/row-fluid-->

<?php endforeach; ?>