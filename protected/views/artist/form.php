<?php
$this->pageTitle = Yii::app ()->name . ' - Artist';

$form = $this->beginWidget ('CActiveForm', array(
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array(
        'name' => 'artist',
        'enctype' => 'multipart/form-data'
    )
)); ?>

<div class="row">
    <?php echo $form->textField($model, 'firstName', array(
        'placeholder' => $model->getAttributeLabel('firstName'),

    )); ?>
    <?php echo $form->error($model,'firstName'); ?>
</div>

<div class="row">
    <?php echo $form->textField($model, 'middleName', array(
        'placeholder' => $model->getAttributeLabel('middleName'),

    )); ?>
    <?php echo $form->error($model,'middleName'); ?>
</div>

<div class="row">
    <?php echo $form->textField($model, 'surName', array(
        'placeholder' => $model->getAttributeLabel('surName'),

    )); ?>
    <?php echo $form->error($model,'surName'); ?>
</div>

<div class="row">
    <?php echo $form->textArea($model, 'description', array(
        'placeholder' => $model->getAttributeLabel('description'),

    )); ?>
    <?php echo $form->error($model,'description'); ?>
</div>

<div class="row">
    <?php echo $form->emailField($model, 'email', array(
        'placeholder' => $model->getAttributeLabel('email'),

    )); ?>
    <?php echo $form->error($model,'email'); ?>
</div>

<div class="row">
    <?php echo $form->textField($model, 'skype', array(
        'placeholder' => $model->getAttributeLabel('skype'),

    )); ?>
    <?php echo $form->error($model,'skype'); ?>
</div>

<div class="row">
    <?php echo $form->textField($model, 'phone', array(
        'placeholder' => $model->getAttributeLabel('phone'),
    )); ?>
    <?php echo $form->error($model,'phone'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'img'); ?>
    <?php echo $form->fileField($model, 'img'); ?>
    <?php echo $form->error($model,'img'); ?>
</div>

<?php echo CHtml::submitButton ('Save'); ?>

<?php $this->endWidget (); ?>