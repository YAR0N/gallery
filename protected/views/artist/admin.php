<?php $this->pageTitle = Yii::app ()->name . ' - Artist'; ?>

<div>
<?php
if(Yii::app()->user->hasFlash('message')) {
   echo CHtml::encode(Yii::app()->user->getFlash('message'));
}
?>
</div>

<?php

echo CHtml::link('Add artist', $this->createUrl('artist/create'));

$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $dataProvider,
    'columns' => array(
        'surName:html',
        'firstName:html',
        'middleName:html',
        array(
            'class'=>'CButtonColumn'
        )
    )
));
?>