<?php $this->pageTitle = Yii::app ()->name . ' - Category'; ?>

<div>
<?php
if(Yii::app()->user->hasFlash('message')) {
   echo CHtml::encode(Yii::app()->user->getFlash('message'));
}
?>
</div>

<?php

echo CHtml::link('Add category', $this->createUrl('category/create'));

$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $dataProvider,
    'columns' => array(
        'name:html',
        array(
            'class'=>'CButtonColumn'
        )
    )
));
?>