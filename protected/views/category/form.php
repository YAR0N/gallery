<?php
$this->pageTitle = Yii::app ()->name . ' - Category';

$form = $this->beginWidget ('CActiveForm', array(
    'enableClientValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
    ),
    'htmlOptions' => array(
        'name' => 'category',
    )
)); ?>

<div class="row">
    <?php echo $form->textField($model, 'name', array(
        'placeholder' => $model->getAttributeLabel('name'),

    )); ?>
    <?php echo $form->error($model,'name'); ?>
</div>

<?php echo CHtml::submitButton ('Save'); ?>

<?php $this->endWidget (); ?>