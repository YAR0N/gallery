<?php $this->pageTitle = Yii::app ()->name . ' - ' . $model->name; ?>

<div class="page-header">
    <h1><?php echo CHtml::encode($model->name) ?></h1>
</div>

<?php $this->renderPartial('//picture/_pictures', array('pictures' => $model->pictures)); ?>

</div><!-- /row-fluid -->