<?php

class ArtistController extends Controller
{

    public function actionAdmin()
    {
        $dataProvider = new EMongoDataProvider('Artist');
        $this->render('admin', array('dataProvider' => $dataProvider));
    }

    public function actionView()
    {
        $model = $this->loadArtistModel();
        if(!is_null($model)) {
            $this->render('view', array('model' => $model));
        } else {
            $this->redirect(array('site/index'));
        }
    }

    public function actionCreate()
    {
        $model = new Artist();

        if (isset($_POST['Artist'])) {
            $model->attributes = $_POST['Artist'];
            if ($model->validate() && $model->save()) {
                Yii::app()->user->setFlash('message', 'Artist saved');
                $this->redirect('admin');
            }
        }

        $this->render('form', array('model' => $model));
    }

    public function actionDelete()
    {
        if(!Yii::app()->request->getIsAjaxRequest() || $_GET['ajax'] !== 'yw0') {
            throw new CHttpException(400, 'Not ajax request');
        }

        $model = $this->loadArtistModel();
        if(!is_null($model)) {
            if(!$model->delete()) {
                throw new CHttpException(500, 'Artist can`t be deleted');
            }
        } else {
            throw new CHttpException(400, 'Invalid request');
        }
    }

    public function actionUpdate()
    {
        $model = $this->loadArtistModel();
        if(is_null($model) && !isset($_POST['Artist'])) {
            Yii::app()->user->setFlash('message', 'Invalid request');
            $this->redirect(array('artist/admin'));
        } elseif (isset($_POST['Artist'])) {
            $model->attributes = $_POST['Artist'];
            //die(var_dump($model));
            if ($model->validate() && $model->save()) {
                Yii::app()->user->setFlash('message', 'Artist saved');
                $this->redirect(array('artist/admin'));
            }
        }

        $this->render('form', array('model' => $model));
    }

    public function actionJsonData()
    {
        if(!Yii::app()->request->getIsAjaxRequest()) {
            throw new CHttpException(400, 'Not ajax request');
        }

        $model = $this->loadArtistModel();
        if(!is_null($model)) {
            echo $model->getJSONDocument();
        } else {
            throw new CHttpException(400, 'Invalid request');
        }
    }

    private function loadArtistModel()
    {
        if(isset($_GET['id'])) {
            $id = $_GET['id'];
            $model = Artist::model()->findBy_id($id);
            if(is_null($model)) {
                throw new CHttpException(404, 'No such artist');
            }
            return $model;
        } else {
            return null;
        }
    }
}