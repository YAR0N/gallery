<?php

class SiteController extends Controller
{
    /**
     * Declares class-based actions.
     */
    public function actions ()
    {
        return array(

            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$pictures = Picture::model()->find()->limit(4);
        $categories = Category::model()->findAll();
        $artists = Artist::model()->findAll();

		$this->render('index', array('pictures' => $pictures, 'categories' => $categories, 'artists' => $artists));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	public function actionImage()
    {
        if(!isset($_GET['id'])) {
            throw new CHttpException(400, 'Image id not included in request');
        }
        $id = $_GET['id'];
        $image = EMongoFile::model()->findOne(array('entityId' => new MongoId($id)));
        if(!is_null($image)) {
            header('Content-type: image/jpeg');
            echo $image->getBytes();
        } else {
            throw new CHttpException(404, 'No such image');
        }
    }
}