<?php

class CategoryController extends Controller
{


    public function actionAdmin()
    {
        $dataProvider = new EMongoDataProvider('Category');
        $this->render('admin', array('dataProvider' => $dataProvider));
    }

    public function actionView()
    {
        $model = $this->loadCategoryModel();
        if(!is_null($model)) {
            $this->render('view', array('model' => $model));
        } else {
            $this->redirect(array('site/index'));
        }
    }

    public function actionCreate()
    {
        $model = new Category();

        if (isset($_POST['Category'])) {
            $model->attributes = $_POST['Category'];
            if ($model->validate() && $model->save()) {
                Yii::app()->user->setFlash('message', 'Category saved');
                $this->redirect('admin');
            }
        }

        $this->render('form', array('model' => $model));
    }

    public function actionDelete()
    {
        if(!Yii::app()->request->getIsAjaxRequest() || $_GET['ajax'] !== 'yw0') {
            throw new CHttpException(400, 'Not ajax request');
        }

        $model = $this->loadCategoryModel();
        if(!is_null($model)) {
            if(!$model->delete()) {
                throw new CHttpException(500, 'Category can`t be deleted');
            }
        } else {
            throw new CHttpException(400, 'Invalid request');
        }
    }

    public function actionUpdate()
    {
        $model = $this->loadCategoryModel();
        if(is_null($model) && !isset($_POST['Category'])) {
            Yii::app()->user->setFlash('message', 'Invalid request');
            $this->redirect(array('category/admin'));
        } elseif (isset($_POST['Category'])) {
            $model->attributes = $_POST['Category'];
            if ($model->validate() && $model->save()) {
                Yii::app()->user->setFlash('message', 'Category saved');
                $this->redirect(array('category/admin'));
            }
        }

        $this->render('form', array('model' => $model));
    }

    private function loadCategoryModel()
    {
        if(isset($_GET['id'])) {
            $id = $_GET['id'];
            $model = Category::model()->findBy_id($id);
            if(is_null($model)) {
                throw new CHttpException(404, 'No such category');
            }
            return $model;
        } else {
            return null;
        }
    }
}