<?php

class PictureController extends Controller
{

    public function actionAdmin()
    {
        $dataProvider = new EMongoDataProvider('Picture');
        $this->render('admin', array('dataProvider' => $dataProvider));
    }

    public function actionView()
    {
        //TODO make view
        $model = $this->loadPictureModel();
        if(!is_null($model)) {
            $this->render('view', array('model' => $model));
        } else {
            $this->redirect(array('site/index'));
        }
    }

    public function actionCreate()
    {
        $model = new Picture();

        if (isset($_POST['Picture'])) {
            $model->attributes = $_POST['Picture'];
            if ($model->validate() && $model->save()) {
                Yii::app()->user->setFlash('message', 'Picture saved');
                $this->redirect('admin');
            }
        }

        $artists = Artist::model()->findAll();
        $categories = Category::model()->findAll();

        $this->render('form', array('model' => $model, 'artists' => $artists, 'categories' => $categories));
    }

    public function actionDelete()
    {
        if(!Yii::app()->request->getIsAjaxRequest() || $_GET['ajax'] !== 'yw0') {
            throw new CHttpException(400, 'Not ajax request');
        }

        $model = $this->loadPictureModel();
        if(!is_null($model)) {
            if(!$model->delete()) {
                throw new CHttpException(500, 'Picture can`t be deleted');
            }
        } else {
            throw new CHttpException(400, 'Invalid request');
        }
    }

    public function actionUpdate()
    {
        $model = $this->loadPictureModel();
        if(is_null($model) && !isset($_POST['Picture'])) {
            Yii::app()->user->setFlash('message', 'Invalid request');
            $this->redirect(array('picture/admin'));
        } elseif (isset($_POST['Picture'])) {
            $model->attributes = $_POST['Picture'];

            if ($model->validate() && $model->save()) {
                Yii::app()->user->setFlash('message', 'Picture saved');
                $this->redirect(array('picture/admin'));
            }
        }

        $artists = Artist::model()->findAll();
        $categories = Category::model()->findAll();

        $this->render('form', array('model' => $model, 'artists' => $artists, 'categories' => $categories));
    }

    private function loadPictureModel()
    {
        if(isset($_GET['id'])) {
            $id = $_GET['id'];
            $model = Picture::model()->findBy_id($id);
            if(is_null($model)) {
                throw new CHttpException(404, 'No such picture');
            }
            return $model;
        } else {
            return null;
        }
    }
}